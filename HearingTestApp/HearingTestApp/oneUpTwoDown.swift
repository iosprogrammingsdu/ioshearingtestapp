//
//  oneUpTwoDown.swift
//  HearingTestApp
//

import AudioKit
import Foundation

class OneUpTwoDown: PureToneScreening {
    
    var thisDB = 0
    var thisFreq = 0
    let baseDB = 60
    let MaxDB = 80
    let MinDB = 5
    let Frequencies = [250, 500, 1000, 2000]
    var resultList = [FreqObservation]()
    var userRegisteredSound = false
    var timerEnabled = true
    var panner: AKPanner?
    var persistence = Persistence()
    
    override func startTest() {
        resultList = [FreqObservation]()
        panner = AKPanner(oscillator)
        TestActive = true
        screenLeftEar()
        screenRightEar()
        persistence.saveResults(incoming : resultList)
        print("Your results are in! Here they are: ")
        print(persistence.retrieveResults())   // Results now
        TestActive = false
    }
    
    func screenLeftEar() {
        panner?.pan = -1
        AudioKit.output = panner
        startAudio()
        screenEar(ear: "Left")
        
    }
    
    func screenRightEar() {
        panner?.pan = 1
        AudioKit.output = panner
        startAudio()
        screenEar(ear: "Right")
    }
    
    func screenEar(ear: String) {
        for currentFreq in Frequencies {
            let treshold = oneUpTwoDownPerDB(currentFreq: currentFreq)
            let result = FreqObservation(ear: ear, freq: currentFreq, dBTreshold: treshold)
            resultList.append(result)
        }
    }
    
    func userRegistered() {
        userRegisteredSound = true
        timerEnabled = false
    }

    @objc func fireTimer() {
        timerEnabled = false
        print("timer fired")
    }
    
    func convertDBToAKAmplitude(db: Int) -> Double {
        let result = Double(db)/10240 //used to be 10^db/20
        print(result)
        return result
    }
    
    func oneUpTwoDownPerDB(currentFreq: Int) -> Int {
        oscillator.frequency = Double(currentFreq)
        thisFreq = currentFreq
        var currentDBLevel = baseDB
        var checkedDBLevels = [DBHitCounter]()
        startAudio()
        
        while (checkForSpecificHits(receivedSet: checkedDBLevels, hit: 3) == nil && TestActive) {
            thisDB = currentDBLevel
            print(currentFreq, currentDBLevel)
            oscillator.amplitude = convertDBToAKAmplitude(db: currentDBLevel)
            startOsc()
            
            timerEnabled = true
            
            let work = DispatchWorkItem(block: {self.fireTimer()})
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 4.0, execute: work)
            while (timerEnabled) {
                if (userRegisteredSound) {
                    work.cancel()
                    print("cancelled timer because user registered sound", currentDBLevel)
                }
            }
            work.cancel()
            stopOsc()
            
            if (userRegisteredSound){ //do this if user heard the sound  //We check if the db level was heard before
                
                if (getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel) == -1){ // if this level isn't heard  before
                    let currentDBHit = DBHitCounter(dBLevel: currentDBLevel)
                    currentDBHit.addHit()
                    checkedDBLevels.append(currentDBHit)
                } else {    //if this level was heard before
                    let index = getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel)
                    checkedDBLevels[index].addHit()
                }
                
                if ((currentDBLevel - 10) >= MinDB) { // goes two down
                    currentDBLevel = currentDBLevel - 10
                } else {
                    currentDBLevel = MinDB
                }
                userRegisteredSound = false
                
            } else {//user didn't hear sound
                if ((currentDBLevel + 5) <= MaxDB) {// adds one up
                    currentDBLevel = currentDBLevel + 5
                } else {
                    currentDBLevel = MaxDB
                    if (getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel) == -1){ // if this level isn't heard  before
                        let currentDBHit = DBHitCounter(dBLevel: currentDBLevel)
                        currentDBHit.addHit()
                        checkedDBLevels.append(currentDBHit)
                    } else {
                        let index = getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel)
                        checkedDBLevels[index].addHit()
                    }
                }
            }
        }
        stopAudio()
        return (checkForSpecificHits(receivedSet: checkedDBLevels, hit: 3)?.db ?? -1)
    }

    
    //checks if a hitcounter has obtained specified hitcount
    func checkForSpecificHits(receivedSet: [DBHitCounter], hit: Int)-> DBHitCounter? {
        for hitCounter in receivedSet {
            if hitCounter.getHits() == hit {
                return hitCounter
            }
        }
        return nil
    }
    
    //returns index for hitcounter with specified DB level
    func getIndexForDB(receivedSet: [DBHitCounter], DB: Int)-> Int {
        var index = 0
        for DBHitCounter in receivedSet {
            if DBHitCounter.getDB() == DB {
                return index
            }
            index = index + 1
        }
        return -1
    }
}
