//
//  PureToneScreening.swift
//  HearingTestApp
//

import Foundation
import AudioKit


class PureToneScreening {
    var oscillator = AKOscillator()
    var MAXFREQ: Int {return 250}
    var MAXAMP: Double {return 0.5}
    var TestActive = false
    
    init() {
        oscillator.frequency = 250
        oscillator.amplitude = 2
        oscillator.rampDuration = 0.001
    }
    
    func startAudio(){
        do {
            try AudioKit.start()
            print("started AudioKit")
        } catch {
            print("failed to start audiokit")
        }
    }
    
    func stopAudio(){
        do {
            try AudioKit.stop()
            print("Stopped AudioKit")
        } catch {
            print("failed to stop audiokit")
        }
    }
    func startOsc() {
        oscillator.start()
    }
    func stopOsc() {
        oscillator.stop()
    }
    
    func oscillatorEnabled() -> Bool {
        return oscillator.isStarted
    }
    func isActive() -> Bool{
        return TestActive
    }
    
    func startTest() {
        // will be inherited and overwritten in OneUpTwoDown
    }
}
