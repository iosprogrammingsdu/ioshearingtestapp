//
//  Persistence.swift
//  HearingTestApp
//

import Foundation


class Persistence {

    var jsonData: Data?

    func saveResults(incoming : [FreqObservation]) {
        let result = retrieveResults() + incoming
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("Results.json")
        jsonData = try? JSONEncoder().encode(result)
        do {
            try jsonData?.write(to: path)
            print("results saved!")
        } catch {
            print("Couldn't write file.")
        }
    
    }
    
    
    
    func retrieveResults() -> [FreqObservation]{
            if let url = try? FileManager.default.url(
                for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent("Results.json") {
                if let jsonData = try? Data(contentsOf: url){
                    if let resultList = try? JSONDecoder().decode(Array<FreqObservation>.self, from: jsonData) {
                        return resultList
                    }
                }
        }
        return []
        
    }
}


