//
//  DBHitCounter.swift
//  HearingTestApp
//

import Foundation

class DBHitCounter {
    var db: Int
    var hits = 0
    
    init(dBLevel: Int) {
        db = dBLevel
    }
    
    func setDB(value: Int) {
        db = value
    }
    func getDB() -> Int {
        return db
    }
    
    func getHits() -> Int{
        return hits
    }
    
    func addHit() {
        hits = hits + 1
    }
}
