//
//  FreqObservation.swift
//  HearingTestApp
//

import Foundation

struct FreqObservation: Codable
{
    var ear: String
    var freq: Int
    var dBTreshold: Int

    
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }
    
    init(ear: String, freq: Int, dBTreshold: Int) {
        self.ear = ear
        self.freq = freq
        self.dBTreshold = dBTreshold
    }
    
    func toString() -> String {
        return "\(ear) \(freq) \(dBTreshold)"
    }
    
}
