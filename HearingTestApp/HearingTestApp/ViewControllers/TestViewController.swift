//
//  TestViewController.swift
//  HearingTestApp
//

import UIKit

class TestViewController: UIViewController {
    var TableData:Array< String > = Array < String >()
    @IBOutlet weak var freqLabel: UILabel!
    
    func updateLabels() {
        freqLabel.text = "Frequency: " + String(screening.thisFreq) + " Hz"
        if screening.thisDB >= 10 {
        volumeLabel.text = "Volume: " + String(screening.thisDB-10) + "dB"
        } else if screening.thisDB <= 40 {
        volumeLabel.text = "Volume: " + String(screening.thisDB) + "dB"
        }
    }
    var soundUnderNoiseTreshold = false
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(false)
        screening.TestActive = false
        stopAudio()
    }
    
    @IBAction func CheckEnvironmentButton() {
        checkEnvironment()
    }
    func checkEnvironment() {
        let envCheck = EnvironmentCheck()
        if (envCheck.check()) {
            soundUnderNoiseTreshold = true
            self.EnvironmentCheckLabel.text = "Clear to perform test"
            StartAfterEnviroCheckButton.isEnabled = true
        } else {
            soundUnderNoiseTreshold = false
            self.EnvironmentCheckLabel.text = "Too loud, find a more quiet place!"
            StartAfterEnviroCheckButton.isEnabled = false
        }
    }
    
    @IBOutlet weak var StartAfterEnviroCheckButton: UIButton!
    @IBOutlet weak var EnvironmentCheckLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    var screening = OneUpTwoDown()
    
    @IBAction func ScreeningButton(_ sender: UIButton) {
        updateLabels()
        if (!screening.TestActive) {
            DispatchQueue.global(qos: .background).async { self.screening.startTest() }
        }
        if(screening.isActive()) {
            sender.setTitle("I heard that", for: .normal)
            screening.userRegistered()
            
        } else {
            sender.setTitle("Start test", for: .normal)
            
        }
    }
    func startOsc(){
        screening.startOsc()
    }
    func stopOsc() {
        screening.stopOsc()
    }
    
    func startAudio() {
        screening.startAudio()
    }
    
    func stopAudio() {
       screening.stopAudio()
    }
     
}
