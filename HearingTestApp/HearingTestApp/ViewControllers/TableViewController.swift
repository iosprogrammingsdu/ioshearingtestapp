import UIKit

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var persistence = Persistence()
    var resultList = [FreqObservation]()
    
    let cellReuseIdentifier = "cell"
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resultList = persistence.retrieveResults()

        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
    }

    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1//self.resultList.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
        
        var resultString = "All results:\n"
        var resultCount = 2

        for element in resultList {
            resultString += element.toString() + "\n"
            resultCount += 1
        }
        
        cell.textLabel?.numberOfLines = resultCount
        cell.textLabel?.text = resultString
        
        return cell
    }
    
}
