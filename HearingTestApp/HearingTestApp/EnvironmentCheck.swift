//
//  EnvironmentCheck.swift
//  HearingTestApp
//

import AudioKit
import Foundation

class EnvironmentCheck {
    
    func check() -> Bool {
        //check if environment is too loud for hearing test.
        //do this by recording and sampling to make a mean DB level, and determine if it is above permitted treshold
        let mic = AKMicrophone()
        
        let tracker = AKFrequencyTracker.init(mic)
        let silence = AKBooster(tracker, gain: 1)
        
        AudioKit.output = silence
        
        do {
            try AudioKit.start()
        } catch {
            print("audiokit didn't start")
        }
        
        var track = true
        var result = 0.0
        
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 2.0) {track = false}
        while (tracker.amplitude < 0.5 && track == true) {
            result = tracker.amplitude
            print(result)
        }
        
        do {
            try AudioKit.stop()
        } catch {
            print("audiokit didn't stop")
        }
        if (result > 0.5) {
            return false
        } else {
            return true}
    }
}
